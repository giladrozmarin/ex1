
const $ = (s, p = document) => p.querySelector(s);
const $$ = (s, p = document) => p.querySelectorAll(s);

// get Image from server 
async function  getImage(current_box,i){
 
let data = await fetch(`https://source.unsplash.com/random?sig=${i}`).then(res =>res.url)
    
let str = `<img src=${data}>`
current_box.innerHTML = str;

}

let gridMasonry =  () => {

   let data =  $$('.box')
  
  for (const [i,box] of data.entries()) { 
    getImage(box,i)
}
    
  
 

}


gridMasonry()
